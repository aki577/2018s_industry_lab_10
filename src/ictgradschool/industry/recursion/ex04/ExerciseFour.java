package ictgradschool.industry.recursion.ex04;

/**
 * Created by anhyd on 27/03/2017.
 */
public class ExerciseFour
    {

    /**
     * Returns the sum of all positive integers between 1 and num (inclusive).
     */
    public int getSum(int num)
        {

        // TODO Implement a recursive solution to this method.
        if (num == 1) {
            return 1;
        }
        return (num + getSum(num - 1));
        }


    /**
     * Returns the smallest value in the given array, between the given first (inclusive) and second (exclusive) indices
     *
     * @param nums        the array
     * @param firstIndex  the inclusive lower index
     * @param secondIndex the exclusive upper index
     */
    public int getSmallest(int[] nums, int firstIndex, int secondIndex)
        {
        firstIndex++;
        if (firstIndex == secondIndex) {
            return nums[secondIndex - 1];
        }
        return Math.min(nums[firstIndex - 1], getSmallest(nums, firstIndex, secondIndex));
        // TODO Implement a recursive solution to this method.
        }

    /**
     * Prints all ints from n down to 1.
     */
    public void printNums1(int n)
        {
        System.out.println(n);
        if (n > 1) {
            printNums1(n - 1);
        }
        // TODO Implement a recursive solution to this method.
        }

    /**
     * Prints all ints from 1 up to n.
     */
    public void printNums2(int n)
        {
        if (n > 1) {
            printNums2(n - 1);
        }
        System.out.println(n);

        // TODO Implement a recursive solution to this method.

        }

    /**
     * Returns the number of 'e' and 'E' characters in the given String.
     *
     * @param input the string to check
     */
    public int countEs(String input)
        {
        int count = 0;
        if (input.equals("")) {
            return 0;
        }
        // TODO Implement a recursive solution to this method.
        if (input.charAt(0) == 'e' || input.charAt(0) == 'E') {
            count++;
        }
        return count + countEs(input.substring(1));
        }

    /**
     * Returns the nth number in the fibonacci sequence.
     */
    public int fibonacci(int n)
        {
        if (n == 1 || n == 0) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }

        // TODO Implement a recursive solution to this method.
        }

    /**
     * Returns true if the given input String is a palindrome, false otherwise.
     *
     * @param input the String to check
     */
    public boolean isPalindrome(String input)
        {
        input = input.replaceAll("\\s", "");
        // TODO Implement a recursive solution to this method.
        if (input.equals("") || input.length() == 1) {
            return true;
        }

        String str = input.substring(1, input.length() - 1);
        return (input.charAt(0) == input.charAt(input.length() - 1)) && isPalindrome(str);

        }

    }
